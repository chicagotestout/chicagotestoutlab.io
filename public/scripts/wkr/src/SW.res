open Js;
open ServiceWorker;
open ServiceWorkerGlobalScope;

/* local bindings */
@bs.new external makeExn: string => exn = "Error";

/* configuration */
let version = "-0.0.11";

let runtimeCacheName = Js.String.concat(version, "chicagotestout-runtime");

/* helper methods */

let fromCache = (req: Fetch.Request.t, cacheName: string):
  Promise.t<Fetch.Response.t> =>
  caches(self) -> CacheStorage.open_(cacheName)
|> Promise.then_(cache => cache -> Cache.Match.withoutOptions(req))
|> Promise.then_(matching => switch(Js.Nullable.toOption(matching)) {
    | Some(m) => Promise.resolve(m);
    | None => Promise.reject(makeExn("no-match"));
  })
;

let fromNetwork = (req:Fetch.Request.t):
  Promise.t<Fetch.Response.t> =>
  Fetch.fetchWithRequest(req);

let addToCache = (req: Fetch.Request.t, cacheName: string):
  Promise.t<unit> => {
  caches(self)
    -> CacheStorage.open_(cacheName)
  |> Promise.then_(cache => cache -> Cache.add(#Request(req)))
};

let runtimeStrategy = (req: Fetch.Request.t, cacheName: string):
  Promise.t<Fetch.Response.t> => {
  
  let result = req -> Fetch.Request.makeWithRequest -> fromNetwork
  |> Promise.catch(_ => {
      "ServiceWorker: The network request failed or timed out: "
      |> Js.String.concat(req -> Fetch.Request.url)
      |> Js.log;
      req -> Fetch.Request.makeWithRequest -> fromCache(cacheName)
    });

  let _ = addToCache(req, cacheName);

  result
};

/* event listeners */
self -> set_oninstall(_ => {
  Js.log("The service worker is being installed.");
});

self -> set_onfetch(event => {
  Js.log("The service worker is serving the asset.");

  let req = event -> FetchEvent.request;
  let resp: Promise.t<Fetch.Response.t> =
    req
    -> Fetch.Request.makeWithRequest
    -> runtimeStrategy(runtimeCacheName);
  event -> FetchEvent.respondWith(#Promise(resp));
});
