open ServiceWorker;
open ServiceWorkerGlobalScope;

let path = "/worker.js";

let _ = self -> navigator
  -> ServiceWorkerNavigator.serviceWorker
  -> ServiceWorkerContainer.Register.withoutOptions(path)
  ;
