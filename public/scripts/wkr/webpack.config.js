const path = require('path');

module.exports = {
    entry: {
        loader: "./lib/js/src/SWLoader.bs.js",
        worker: "./lib/js/src/SW.bs.js"
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "../../"),
    }
};
