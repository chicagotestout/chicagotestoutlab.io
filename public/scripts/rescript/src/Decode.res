open Belt;

/* constants */
type decoder<'result> = (Js.Dict.t<Js.Json.t>, string)
  => Result.t<'result, string>;

let typeError = (type_: string, prop: string): string
  => "Parse Error: "
|> Js.String.concat(prop)
|> Js.String.concat(" not ")
|> Js.String.concat(type_)
;


/* general utilities */

let toOption = (result: Result.t<'t, 'e>): option<'t>
  => switch result {
  | Error(_) => None;
  | Ok(t) => Some(t)
};

let toResult = (op: option<'a>, err: 'b): Result.t<'a, 'b>
  => switch op {
  | None => Result.Error(err);
  | Some(x) => Result.Ok(x);
};

let mapTogether = (first: Result.t<'a, 'error>,
                   second: Result.t<'b, 'error>,
                   func: ('a, 'b) => 'c): Result.t<'c, 'error>
  => Result.flatMap(first, f => Result.map(second, s => func(f, s)));

/* parse utilities */

let failNaN = (number: float): Result.t<float, string> => {
  if Js.Float.isNaN(number) { Result.Error("Parse Error: yielded NaN") }
    else { Result.Ok(number) }
};

let getProp = (dict: Js.Dict.t<Js.Json.t>, prop: string):
  Result.t<Js.Json.t, string>
  => Js.Dict.get(dict, prop)
  -> toResult(Js.String.concat(prop, "Parse Error: property not found: "));  

let posixFromString = (str: string): Result.t<float, string>
  => Js.Date.fromString(str) -> Js.Date.getTime -> failNaN;

/* parsers */

let posix = (dict: Js.Dict.t<Js.Json.t>, prop: string): Result.t<float, string>
  => getProp(dict, prop)
  -> Result.map(json => Js.Json.decodeString(json))
  -> Result.flatMap(op => toResult(op, "Parse Error: date not string."))
  -> Result.flatMap(str => posixFromString(str))
  ;

let number = (dict: Js.Dict.t<Js.Json.t>, prop: string):
  Result.t<float, string>
  => getProp(dict, prop)
  -> Result.map(json => Js.Json.decodeNumber(json))
  -> Result.flatMap(op => toResult(op, typeError("number", prop)))
  ;

let integer = (dict: Js.Dict.t<Js.Json.t>, prop: string):
  Result.t<int, string>
  => switch number(dict, prop) {
  | Result.Ok(n) =>
  if Int.toFloat(Int.fromFloat(n)) === n
    { Result.Ok(Int.fromFloat(n)) }
    else { Result.Error("Parse error: number not integer. ") }
  | Result.Error(e) => Result.Error(e)
};

let str = (dict: Js.Dict.t<Js.Json.t>, prop: string): Result.t<string, string>
  => getProp(dict, prop)
  -> Result.map(json => Js.Json.decodeString(json))
  -> Result.flatMap(op => toResult(op, typeError("string", prop)))
  ;


let numeric = (dict: Js.Dict.t<Js.Json.t>, prop: string):
  Result.t<float, string>
  => str(dict, prop)
  -> Result.flatMap(str => switch Belt.Float.fromString(str) {
  | Some(number) => Belt.Result.Ok(number);
  | None => Belt.Result.Error("could not parse number from: "
                              |> Js.String.concat(str));
});

/* pipes */

let calc = (t: Result.t<'t, string>, formula: 't => 't): Result.t<'t, string>
  => switch t {
  | Ok(obj) => Result.Ok(formula(obj));
  | Error(str) => Result.Error(str);
};

let opt = (t: Belt.Result.t<'t, string>,
           prop: string,
           decode: ((Js.Dict.t<Js.Json.t>, string) =>
                    Result.t<'prop, string>),
           dict: Js.Dict.t<Js.Json.t>,
           update: ('t, option<'prop>) => 't): Result.t<'t, string>
  => switch t {
  | Error(str) => Result.Error(str);
  | Ok(obj) => Result.Ok(decode(dict, prop) -> toOption |> update(obj));
};


let req = (t: Belt.Result.t<'t, string>,
           prop: string,
           decode: ((Js.Dict.t<Js.Json.t>, string) =>
                     Result.t<'prop, string>),
           dict: Js.Dict.t<Js.Json.t>,
           update: ('t, 'prop) => 't): Result.t<'t, string>
  => mapTogether(t, decode(dict, prop), update);

