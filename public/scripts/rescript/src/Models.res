type rawByTest = {
  dateStr: string,
  posix: float,
  day: string,
  positive: float,
  total: float,
  percent: float
};


/* daily rolling average of cases per 100,000 Chicagoans */
type rawRollingCaseRate = {
  dateStr: string,
  posix: float,
  caseRate: float
};

module DTO = {
  type byTest = {
    dateStr: string,
    posix: float,
    day: string,
    positive: float,
    total: float,
    percent: float,
    perCapita: float
  };

  type hospitalization = {
    date_string: string,
    date: float, //posix
    ventilatorsTotalCapacity: float,
    ventilatorsTotal: float,
    ventilatorsCovid19: float,
    icuBedsTotalCapacity: float,
    icuBedsTotal: float,
    icuBedsCovid19: float,
    acuteNonIcuBedsTotalCapacity: float,
    acuteNonIcuBedsTotal: float,
    acuteNonIcuBedsCovid19: float,
  };

  type vaccineAgeGroup = {
    firstDose: float,
    finalDose: float
  };

  type vaccineGroup = {
    rawDoses: float, // total_doses_daily
    rawFirstDoses: float, // _1st_dose_daily
    rawFinalDoses: float, // vaccine_series_completed_daily
    percentFirst: float, // _1st_dose_percent_population
    percentFinal: float, // vaccine_series_completed_percent_population
  };

  type vaccineDay = {
    dateStr: string,
    date: float,
    residents: vaccineGroup, 
    totalVolume: float,
    under18first: float,
    under18final: float,
    from18to29first: float,
    from18to29final: float,
    from30to39first: float,
    from30to39final: float,
    from40to49first: float,
    from40to49final: float,
    from50to59first: float,
    from50to59final: float,
    from60to69first: float,
    from60to69final: float,
    from70to79first: float,
    from70to79final: float,
    over80first: float,
    over80final: float
  };
};
