open Belt.Float; // for * multiplication and / division

let flatLog = (accumulator: array<'t>, item: Belt.Result.t<'t, 'error>) => {
  switch item {
  | Belt.Result.Ok(ok) => accumulator |> Js.Array.concat( [ ok ] )
  | Belt.Result.Error(error) => {
      Js.log(error);
      accumulator
    };
  }
};


module ByTest = {

  let initialize: Models.rawByTest = {
    dateStr: "",
    posix: 0.,
    day: "",
    positive: 0.,
    total: 0.,
    percent: 0.
  };

  let parseRawByTest =
    (json: Js.Json.t): Belt.Result.t<Models.rawByTest, string>
    =>
    switch Js.Json.classify(json) {
    | Js.Json.JSONObject(dict) => Belt.Result.Ok(initialize)
      -> Decode.req("date", Decode.str, dict, (obj, dateStr) => {
      ...obj,
      dateStr: dateStr |> Js.String.substring(~from=0, ~to_=10) }) //strip time
      -> Decode.req("date", Decode.posix, dict, (obj, posix) => {
      ...obj, posix: posix })
      -> Decode.req("day", Decode.str, dict, (obj, day) => {...obj, day: day })
      -> Decode.req("positive_tests", Decode.numeric, dict, (obj, positive) =>
                    {
        ...obj,
        positive: positive })
      -> Decode.req("total_tests", Decode.numeric, dict, (obj, total) => {
      ...obj,
      total: total })
      -> Decode.calc((obj) => {
      ...obj, percent: obj.positive / obj.total * 100. })
    | _ => Belt.Result.Error("Parse error: not an object. ");
    };

    let parseList = (json: Js.Json.t):
    Belt.Result.t<array<Models.rawByTest>, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONArray(jsons)
      => Belt.Result.Ok(jsons
                        |> Js.Array.map(parseRawByTest)
                        |> Js.Array.reduce(flatLog, [])
      );
    | _ => Belt.Result.Error("Parse issue: root not array. ")
  };

};

module Hospitalization = {
  
  let initialize: Models.DTO.hospitalization = {
    date_string: "Unknown",
    date: 0., //posix
    ventilatorsTotalCapacity: 0.,
    ventilatorsTotal: 0.,
    ventilatorsCovid19: 0.,
    icuBedsTotalCapacity: 0.,
    icuBedsTotal: 0.,
    icuBedsCovid19: 0.,
    acuteNonIcuBedsTotalCapacity: 0.,
    acuteNonIcuBedsTotal: 0.,
    acuteNonIcuBedsCovid19: 0.,
  };
  
  let parseHospitalization = (json: Js.Json.t):
  Belt.Result.t<Models.DTO.hospitalization, string>
  =>
  switch Js.Json.classify(json) {
  | Js.Json.JSONObject(dict) => Belt.Result.Ok(initialize)
    -> Decode.req("date", Decode.str, dict, (obj, dateStr) => {
    ...obj,
    date_string: dateStr |> Js.String.substring(~from=0, ~to_=10) })
    -> Decode.req("date", Decode.posix, dict, (obj, posix) => {
    ...obj,
    date: posix })
    -> Decode.req("ventilators_total_capacity", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      ventilatorsTotalCapacity: n })
    -> Decode.req("ventilators_in_use_total", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      ventilatorsTotal: n })
    -> Decode.req("ventilators_in_use_covid_19", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      ventilatorsCovid19: n })
    -> Decode.req("icu_beds_total_capacity", Decode.numeric, dict,
                  (obj, n) => {
    ...obj,
    icuBedsTotalCapacity: n })
    -> Decode.req("icu_beds_in_use_total", Decode.numeric, dict, (obj, n) => {
    ...obj,
    icuBedsTotal: n })
    -> Decode.req("icu_beds_in_use_covid_19", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      icuBedsCovid19: n })
    -> Decode.req("acute_non_icu_beds_total_capacity", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      acuteNonIcuBedsTotalCapacity: n })
    -> Decode.req("acute_non_icu_beds_in_use_total", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      acuteNonIcuBedsTotal: n })
    -> Decode.req("acute_non_icu_beds_in_use_covid_19", Decode.numeric, dict,
                  (obj, n) => {
      ...obj,
      acuteNonIcuBedsCovid19: n })
  | _ => Belt.Result.Error("Parse error: not an object. ");
  };

  let parseList = (json: Js.Json.t):
    Belt.Result.t<array<Models.DTO.hospitalization>, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONArray(jsons)
      => Belt.Result.Ok(jsons |> Js.Array.map(parseHospitalization)
                        |> Js.Array.reduce(flatLog, []))
    | _ => Belt.Result.Error("Parse error: root not array")
  };
  
};

module RollingCaseRate = {

  let initializeRollingCaseRate: Models.rawRollingCaseRate = {
    dateStr: "",
    posix: 0.,
    caseRate: 0.
  };

  let parseRollingCaseRate =
    (json: Js.Json.t): Belt.Result.t<Models.rawRollingCaseRate, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONObject(dict) => Belt.Result.Ok(initializeRollingCaseRate)
    -> Decode.req("date", Decode.str, dict, (obj, dateStr) => {
      ...obj,
      dateStr: dateStr |> Js.String.substring(~from=0, ~to_=10)}) //strip time.
      -> Decode.req("date", Decode.posix, dict, (obj, posix) => {
      ...obj, posix: posix })
      -> Decode.req("cases_rate_total", Decode.numeric, dict,
                    (obj, caseRate) => {
        ...obj,
        caseRate: caseRate })
    | _ => Belt.Result.Error("Parse error: not an object. ");
  };

  let parseList = (json: Js.Json.t):
    Belt.Result.t<array<Models.rawRollingCaseRate>, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONArray(jsons)
      => Belt.Result.Ok(jsons
                        |> Js.Array.map(parseRollingCaseRate)
                        |> Js.Array.reduce(flatLog, [])
      )
    | _ => Belt.Result.Error("Parse issue: root not array. ")
  };

};

module Vaccine = {
  
  let initialVaccineGroup: Models.DTO.vaccineGroup = {
    rawDoses: 0.,
    rawFirstDoses: 0.,
    rawFinalDoses: 0.,
    percentFirst: 0.,
    percentFinal: 0.,
  };
  
  
  let initialVaccine: Models.DTO.vaccineDay = {
    dateStr: "",
    date: 0., 
    residents: initialVaccineGroup, 
    totalVolume: 0.,
    under18first: 0.,
    under18final: 0.,
    from18to29first: 0.,
    from18to29final: 0.,
    from30to39first: 0.,
    from30to39final: 0.,
    from40to49first: 0.,
    from40to49final: 0.,
    from50to59first: 0.,
    from50to59final: 0.,
    from60to69first: 0.,
    from60to69final: 0.,
    from70to79first: 0.,
    from70to79final: 0.,
    over80first: 0.,
    over80final: 0.,
  };
  
  let parseVaccine = (json: Js.Json.t):
    Belt.Result.t<Models.DTO.vaccineDay, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONObject(dict) => Belt.Result.Ok(initialVaccine)
      -> Decode.req("date", Decode.str, dict, (obj, dateStr) => {
      ...obj, dateStr: dateStr
      |> Js.String.substring(~from=0, ~to_=10)}) //strip time.
      -> Decode.req("date", Decode.posix, dict, (obj, posix) => {
      ...obj, date: posix })
      -> Decode.req("total_doses_daily", Decode.numeric, dict, (obj, n) => {
      ...obj, residents: { ...obj.residents, rawDoses: n }})
      -> Decode.req("_1st_dose_daily", Decode.numeric, dict, (obj, n) => {
      ...obj, residents: { ...obj.residents, rawFirstDoses: n }})
      -> Decode.req("vaccine_series_completed_daily", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, residents: { ...obj.residents, rawFinalDoses: n }})
      -> Decode.req("_1st_dose_percent_population", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, residents: { ...obj.residents, percentFirst: n * 100. }})
      -> Decode.req("vaccine_series_completed_percent_population",
                    Decode.numeric, dict, (obj, n) => {
        ...obj, residents: { ...obj.residents, percentFinal: n * 100. }})
      -> Decode.req("total_doses_cumulative", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, totalVolume: n })
      -> Decode.req("_1st_dose_daily_age_0_17", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, under18first: n })
      -> Decode.req("vaccine_series_completed_daily_age_0_17", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, under18final: n })
      -> Decode.req("_1st_dose_daily_age_18_29", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from18to29first: n })
      -> Decode.req("vaccine_series_completed_daily_age_18_29", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from18to29final: n })
      -> Decode.req("_1st_dose_daily_age_30_39", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from30to39first: n })
      -> Decode.req("vaccine_series_completed_daily_age_30_39", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from30to39final: n })
      -> Decode.req("_1st_dose_daily_age_40_49", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from40to49first: n })
      -> Decode.req("vaccine_series_completed_daily_age_40_49", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from40to49final: n })
      -> Decode.req("_1st_dose_daily_age_50_59", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from50to59first: n })
      -> Decode.req("vaccine_series_completed_daily_age_50_59", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from50to59final: n })
      -> Decode.req("_1st_dose_daily_age_60_69", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from60to69first: n })
      -> Decode.req("vaccine_series_completed_daily_age_60_69", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from60to69final: n })
      -> Decode.req("_1st_dose_daily_age_70_79", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, from70to79first: n })
      -> Decode.req("vaccine_series_completed_daily_age_70_79", Decode.numeric,
                    dict, (obj, n) => {
        ...obj, from70to79final: n })
      -> Decode.req("_1st_dose_daily_age_80_", Decode.numeric, dict,
                    (obj, n) => {
        ...obj, over80first: n })
      -> Decode.req("vaccine_series_completed_daily_age_80_plus",
                    Decode.numeric, dict, (obj, n) => {
        ...obj, over80final: n })
    | _ => Belt.Result.Error("Parse error: root not object");
    };

  let parseList = (json: Js.Json.t):
    Belt.Result.t<array<Models.DTO.vaccineDay>, string>
    => switch Js.Json.classify(json) {
    | Js.Json.JSONArray(jsons)
      => Belt.Result.Ok(jsons
                        |> Js.Array.map(parseVaccine)
                        |> Js.Array.reduce(flatLog, [])
      )
      | _ => Belt.Result.Error("Parse issue: root not array. ")
  };
  
}; //end vaccine

