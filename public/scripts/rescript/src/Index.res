open Elm;
open Js;

let byTestUrl: string =
  "https://data.cityofchicago.org/resource/gkdw-2tgv.json";

let hospitalizationEndpoint: string =
  "https://data.cityofchicago.org/resource/f3he-c6sv.json";

let rollingCaseRateUrl: string =
  "https://data.cityofchicago.org/resource/e68t-c7fv.json";

let vaccineUrl: string =
  "https://data.cityofchicago.org/resource/2vhs-cf6b.json";

@bs.val external reject: string => Promise.t<'t> = "reject";

let resolveResult = (result: Belt.Result.t<'t, string>): Promise.t<'t> =>
  switch result {
  | Belt.Result.Ok(t) => Promise.resolve(t)
  | Belt.Result.Error(str) => reject(str)
  };

@bs.val @bs.scope("document")
  external getElementById: string => Dom.element = "getElementById";

module Ports = {
  type t = {
    updateByTests: Elm.sendable<array<Models.DTO.byTest>>,
    updateHospitalizations: Elm.sendable<array<Models.DTO.hospitalization>>,
    updateVaccines: Elm.sendable<array<Models.DTO.vaccineDay>>,
  };
};

let fetchRollingCaseRates: Promise.t<array<Models.rawRollingCaseRate>> =
  Remote.fetchJson(rollingCaseRateUrl) |> Promise.then_(json =>
    switch Parsing.RollingCaseRate.parseList(json) {
    | Belt.Result.Ok(rates) => Promise.resolve(rates)
    | Belt.Result.Error(ex) => reject(ex)
    }
);

let combine = (byTest: Models.rawByTest, rate: Models.rawRollingCaseRate):
  Models.DTO.byTest => {
  dateStr: byTest.dateStr,
  posix: byTest.posix,
  day: byTest.day,
  positive: byTest.positive,
  total: byTest.total,
  percent: byTest.percent,
  perCapita: rate.caseRate,
};

let app: Elm.app<Ports.t> =
  Elm.Main.init({node: getElementById("elm-target")});

let _: Promise.t<unit> =
  Remote.fetchJson(hospitalizationEndpoint)
|> Promise.then_(json => Parsing.Hospitalization.parseList(json)
                 |> resolveResult)
|> Promise.then_(hosps => app.ports.updateHospitalizations.send(hosps)
                 |> Promise.resolve);

let resolveJoint = (byTests: array<Models.rawByTest>): Promise.t<
  array<ArrayHelper.joint<Models.rawByTest, Models.rawRollingCaseRate>>,
> =>
  fetchRollingCaseRates |> Promise.then_(rates =>
    ArrayHelper.join(byTests, rates, (b, r) => b.Models.posix === r.Models.posix) |> Promise.resolve
);

let _: Promise.t<unit> =
  Remote.fetchJson(byTestUrl)
|> Promise.then_(json => Parsing.ByTest.parseList(json) |> Promise.resolve)
|> Promise.then_(byTests =>
                 switch byTests {
                 | Belt.Result.Ok(bs) => resolveJoint(bs)
                 | Belt.Result.Error(str) => reject(str)
                 }
  )
|> Promise.then_(joints => joints
                 |> Array.map(joint =>
                              combine(joint.ArrayHelper.x, joint.ArrayHelper.y))
                 |> Promise.resolve
  )
|> Promise.then_(days => app.ports.updateByTests->Elm.send(days)
                 |> Promise.resolve)
;

let _: Promise.t<unit> =
  Remote.fetchJson(vaccineUrl)
|> Promise.then_(json => Parsing.Vaccine.parseList(json) |> resolveResult)
|> Promise.then_(vaccines => {
    Js.log("Vaccines length: ");
    Js.log(Js.Array.length(vaccines));
    app.ports.updateVaccines -> Elm.send(vaccines)
    |> Promise.resolve
  })
;

