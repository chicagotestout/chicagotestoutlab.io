type joint<'x, 'y> = { x: 'x, y: 'y };

let reducer = (x: 'x, ys: array<'y>, condition: ('x, 'y) => bool):
  array<joint<'x, 'y>>
  => Js.Array.filter(y => condition(x, y), ys)
|> Js.Array.map(y => {x: x, y: y })
;

let join = (xs: array<'x>, ys: array<'y>, condition: ('x, 'y) => bool):
  array<joint<'x, 'y>>
  => Js.Array.reduce((results, x) => reducer(x, ys, condition)
                           -> Js.Array.concat(results), [], xs);
