
let fetchJson = (url: string): Js.Promise.t<Js.Json.t> =>
  Fetch.fetch(url)
|> Js.Promise.then_(Fetch.Response.text)
|> Js.Promise.then_(text => Js.Json.parseExn(text) |> Js.Promise.resolve)
;
                                       

