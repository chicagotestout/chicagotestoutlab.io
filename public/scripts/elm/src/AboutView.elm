module AboutView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (Msg)

root : Html Msg
root =
    div [ class "padded" ]
        [ p [ class "p" ]
              [ a [ href sourceUrl, rel "noopener", target "_blank" ]
                    [ text "Full source, documentation, and project roadmap"
                    ]
              , text " available on "
              , text "GitLab" 
              , text ". "
              , a [ href articleUrl, rel "noopener", target "_blank" ]
                  [ text "Most recent release notes" ]
              , text " available on my blog. "
              ]
        ]

articleUrl: String
articleUrl =
    "https://webbureaucrat.gitlab.io/posts/tracking-COVID-19-vaccinations-in-chicago-release-notes/"
sourceUrl: String
sourceUrl = "https://gitlab.com/chicagotestout/chicagotestout.gitlab.io/"

                        
