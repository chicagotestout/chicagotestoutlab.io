{- represents the state associated with the "COVID-19 Daily Testing - By Test"
dataset. Other testing data is from the "COVID-19 Daily Testing - By Person"
dataset.
-}

module ByTest exposing (TestDay, TestModel, decoder, init)

import Json.Decode exposing (Decoder, float, string)
import Json.Decode.Pipeline

--root to be member of the main model
type alias TestModel =
    { days: List TestDay    
    }

--"by test" row
type alias TestDay =
    { dateString: String
    , date: Float
    , day: String --day of the week
    , positive: Float
    , total: Float --total test volume
    , percent: Float
    , perCapita: Float
    }

init: TestModel
init =
    { days = []
    }

decoder: Decoder TestDay
decoder = Json.Decode.succeed TestDay
        |> Json.Decode.Pipeline.required "dateStr" string
        |> Json.Decode.Pipeline.required "posix" float
        |> Json.Decode.Pipeline.required "day" string
        |> Json.Decode.Pipeline.required "positive" float
        |> Json.Decode.Pipeline.required "total" float
        |> Json.Decode.Pipeline.required "percent" float
        |> Json.Decode.Pipeline.required "perCapita" float
