module Msg exposing (..)

import Hospitalization exposing (Hospitalization)
import Http
import Json.Encode
import Models exposing (Mode(..))
import RawTestDay exposing (RawTestDay)
import Time

type Msg = SetMode Mode
    | UpdateByTest Json.Encode.Value
    | UpdateHospitalizationDays Json.Encode.Value
    | UpdateVaccines Json.Encode.Value
    | UpdateZone Time.Zone
