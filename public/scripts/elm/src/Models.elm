module Models exposing (..) --todo

import ByTest exposing (TestModel)
import Hospitalization exposing (Hospitalization)
--import TestDay exposing (TestDay) --"by person" dataset
import Time
import Vaccine exposing (Vaccine)

-- TODO this naming convention (or lack thereof) is a mess.
type alias Model =
    { byTest: TestModel
    --, days : List TestDay
    , hospitalizations : List Hospitalization
    , mode : Mode
    , vaccine : Vaccine
    , zone: Time.Zone
    }

type Mode =  ByTest | Hospitalization | Vaccine

init : Model
init =
    { byTest = ByTest.init
    --, days = []
    , hospitalizations = []
    , mode = ByTest
    , vaccine = Vaccine.init
    , zone = Time.utc
    }

