module VaccineView exposing (root)

import Models exposing(..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)
import VaccineDashboardView
import VaccineDoseChart
import VaccineTable

root : Model -> Html Msg
root model = 
    div [ class "div-vaccine" ]
        [ div [ class "padded" ]
              [ h2 [ class "h2" ] [ text "Vaccinations" ]
              , p [ class "p" ]
                  [ text "This includes Chicago residents only and not "
                  , text "vaccinations of nonresidents that occur within "
                  , text "the city."
                  ]
              , p [ class "p" ]
                  [ text "Source: "
                  , a [ href vaccinationsUrl, target "_blank", rel "noopener" ]
                      [ Html.cite []
                            [ text "COVID-19 Daily Vaccinations "
                            , text "- Chicago Residents"
                            ]
                      ]
                  ]
              , hr [ class "hr-spacer" ][]
              , VaccineDashboardView.root model
              , VaccineDoseChart.root model
              ]
        , VaccineTable.root model
        ]

vaccinationsUrl: String
vaccinationsUrl = "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Vaccinations-Chicago-Residents/2vhs-cf6b"
