module RollingRateChart exposing (root)

import ByTest exposing (TestDay)
import Html exposing (..)
import Html.Attributes exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Axis.Line as AxisLine
import LineChart.Axis.Range as Range
import LineChart.Axis.Ticks as Ticks
import LineChart.Axis.Title as Title
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import Models exposing (Model)
import Time


type alias ChartModel =
    { date: Float
    , val: Float
    }

root: Model -> Html Msg
root model =
    div [ class "div-chart" ]
        [ h3 [] [ text "Rolling Average Case Rate" ]
        , p []
            [ text "The seven-day rolling average of COVID-19 cases per "
            , text "100,000 Chicagoans. "
            ]
        , p []
            [ text "Source: "
            , a [ href rollingUrl, target "_blank", rel "noopener" ]
                [ Html.cite [] [ text "COVID-19 Daily Rolling Average "
                               , text "Case and Death Rates"
                               ]
                ]
            ]
        , chart model.zone model.byTest.days
        ]

xAxisConfig : Time.Zone -> Axis.Config ChartModel msg
xAxisConfig zone =
    Axis.custom
        { title = Title.default "Time"
        , variable = Just << .date
        , pixels = 1000
        , range = Range.default
        , axisLine = AxisLine.rangeFrame Colors.black
        , ticks = Ticks.time zone 16
        }

chart : Time.Zone -> List TestDay -> Html Msg
chart zone days =
    LineChart.viewCustom
        { x = xAxisConfig zone
        , y = Axis.default 400 "Cases" .val
        , area = Area.default
        , container = Container.default "rolling-rate-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.none
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.wider 2
        , dots = Dots.default
        }
        [ LineChart.line Colors.red Dots.none
              "rolling average case rate" (points days)
        ]

points: List TestDay -> List ChartModel
points days =
    List.map (\d -> ChartModel d.date d.perCapita) days


rollingUrl: String
rollingUrl = "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Rolling-Average-Case-and-Death-Rate/e68t-c7fv"
