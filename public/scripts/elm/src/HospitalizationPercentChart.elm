module HospitalizationPercentChart exposing (chart)

import Hospitalization exposing (Hospitalization)
import Html exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Axis.Line as AxisLine
import LineChart.Axis.Range as Range
import LineChart.Axis.Ticks as Ticks
import LineChart.Axis.Title as Title
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import Time

type alias ChartModel =
    { date: Float
    , val: Float
    }

xAxisConfig : Time.Zone -> Axis.Config ChartModel msg
xAxisConfig zone =
    Axis.custom
        { title = Title.default "Time"
        , variable = Just << .date
        , pixels = 1000
        , range = Range.default
        , axisLine = AxisLine.rangeFrame Colors.black
        , ticks = Ticks.time zone 7
        }

chart : Time.Zone -> List Hospitalization -> Html Msg
chart zone days =
    LineChart.viewCustom
        { x = xAxisConfig zone
        , y = Axis.default 400 "Percent" .val
        , area = Area.default
        , container = Container.default "hospitalization-raw-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.default
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.wider 3
        , dots = Dots.default
        }
        [ LineChart.line Colors.blue Dots.none "ICU Beds" (icuBeds days)
        , LineChart.line Colors.strongBlue Dots.none "Acute Non-ICU Beds"
            (nonIcuBeds days)
        , LineChart.line Colors.red Dots.none "Ventilators" (ventilators days)
        ]

icuBeds: List Hospitalization -> List ChartModel
icuBeds days =
    List.map (\d -> d.icuBedsTotal/d.icuBedsTotalCapacity
                    |> \ratio -> ratio * 100
                    |> ChartModel  d.date)
                  days

nonIcuBeds: List Hospitalization -> List ChartModel
nonIcuBeds days =
    List.map (\d -> d.acuteNonIcuBedsTotal/d.acuteNonIcuBedsTotalCapacity
                  |> \ratio -> ratio * 100
                  |> ChartModel d.date
             )
        days

ventilators : List Hospitalization -> List ChartModel
ventilators days =
    List.map (\d -> d.ventilatorsTotal/d.ventilatorsTotalCapacity
                    |> \ratio -> ratio * 100
                    |> ChartModel d.date)
                  days
