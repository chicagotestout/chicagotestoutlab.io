module ToolbarView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model, Mode(..))
import Msg exposing (..)

root : Model -> Html Msg
root model =
    div [ class "div-toolbar flex-row" ]
        [ label [ class (labelClass model ByTest), title "Tests and Cases" ]
            [ input [ onClick (SetMode ByTest)
                    , type_ "radio"
                    ] []
            , text "🧪"
            ]
        , label [ class (labelClass model Hospitalization)
                , title "Hospitalizations"
                ]
            [ input [ class ""
                    , onClick (SetMode Hospitalization)
                    , type_ "radio"
                    ] []
            , text "🏥"
            ]
        , label [ class (labelClass model Vaccine), title "Vaccines" ]
            [ input [ class ""
                    , onClick (SetMode Vaccine)
                    , type_ "radio"
                    ] []
            , text "💉"
            ]
        ]

labelClass : Model -> Mode -> String
labelClass model mode =
    if model.mode == mode then
        "flex-1 radio-toolbar radio-toolbar-selected"
    else "flex-1 radio-toolbar"
