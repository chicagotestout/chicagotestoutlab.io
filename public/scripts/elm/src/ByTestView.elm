module ByTestView exposing (root)

import ByTestTable exposing(root)
import Models exposing(..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)
import PercentPositivityChart
import RollingRateChart

root : Model -> Html Msg
root model = 
    div [ class "div-by-test" ]
        [ div [ class "padded" ] 
              [ h2 [] [ text "Testing and Cases" ]
              , RollingRateChart.root model
              , PercentPositivityChart.root model
              ]
        , ByTestTable.root model
        ]

byTestUrl : String
byTestUrl = "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Testing-By-Test/gkdw-2tgv"

rollingUrl: String
rollingUrl = "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Rolling-Average-Case-and-Death-Rate/e68t-c7fv"
