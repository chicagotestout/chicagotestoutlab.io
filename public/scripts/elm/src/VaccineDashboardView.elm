module VaccineDashboardView exposing (root)

import AgeGroup
import Format
import FormatNumber
import FormatNumber.Locales
import Models exposing(..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)
import Vaccine exposing (VaccineDay)


root: Model -> Html Msg
root model =
    div [ ]
        [ case  model.vaccine.today of
              Just today -> dashboard today
              Nothing -> text ""
        ]
------------------------------------
ageGroup: String -> Float -> Float -> Html Msg
ageGroup ageGroupName firstDose finalDose =
    div [ class "div-age-group" ]
        [ h4 [ class "h4" ] [ text ageGroupName ]
        , div [ class "div-percent-progress" ]
            [ h5 [ class "h5" ] [ text "First Dose" ]
            , progress
                  [ class "progress"
                  , Html.Attributes.max "100"
                  , String.fromFloat firstDose |> value
                  ][]
            , text " "
            , Format.toDecimals 1 firstDose |> text
            , text "%"
            ]
        , div [ class "div-percent-progress" ]
            [ h5 [ class "h5" ] [ text "Final Dose" ]
            , progress
                [ class "progress"
                , Html.Attributes.max "100"
                , String.fromFloat finalDose |> value
                ][]
            , text " "
            , Format.toDecimals 1 finalDose |> text
            , text "%"
            ]
        ]

dashboard: VaccineDay -> Html Msg
dashboard today =
    div [ class "div-vaccine-dashboard" ]
        [ h3 [ class "h3" ]
          [ text "Cumulative Vaccine Doses: "
          , today.totalVolume |> Format.toDecimals 0 |> text
          ]
        , hr [ class "hr-spacer" ][]
        , h3 [ class "h3" ] [ text "Percent of Chicagoans Vaccinated" ]
        , div [ class "div-percent-progress" ]
            [ h4 [ class "h4" ] [ text "First Dose" ]
            , progress
                  [ class "progress"
                  , Html.Attributes.max "100"
                  , today.residents.percentFirst |> String.fromFloat |> value
                  ] []
            , today.residents.percentFirst |> Format.toDecimals 1 |> text
            , text "%"
            ]
        , div [ class "div-percent-progress" ]
            [ h4 [ class "h4" ] [ text "Final Dose" ]
            , progress
                  [ class "progress"
                  , Html.Attributes.max "100"
                  , today.residents.percentFinal |> String.fromFloat |> value
                  ] []
            , today.residents.percentFinal |> Format.toDecimals 1 |> text
            , text "%"
            ]
        , hr [ class "hr-spacer" ][]
        , h3 [ class "h3" ] [ text "Percent Vaccinated by Age" ]
        , p [ class "p" ]
            [ text "These percentages are based on the city's population "
            , text "totals commonly used in other datasets. "
            ]
        , p [ class "p" ]
            [ text "Source: "
            , a [ href populationCountsUrl, target "_blank", rel "noopener" ]
                [ Html.cite [] [ text "Chicago Population Counts" ]
                ]
            ]
        , ageGroup "Under 18" today.under18first today.under18final
        , ageGroup "Ages 18-29" today.from18to29first today.from18to29final
        , ageGroup "Ages 30-39" today.from30to39first today.from30to39final
        , ageGroup "Ages 40-49" today.from40to49first today.from40to49final
        , ageGroup "Ages 50-59" today.from50to59first today.from50to59final
        , ageGroup "Ages 60-69" today.from60to69first today.from60to69final
        , ageGroup "Ages 70-79" today.from70to79first today.from70to79final
        , ageGroup "Over 80" today.over80first today.over80final
        ]


populationCountsUrl: String
populationCountsUrl =
    "https://data.cityofchicago.org/Health-Human-Services/Chicago-Population-Counts/85cm-7uqa"
