module Vaccine exposing ( Vaccine, VaccineDay, decoder, init, update)

import AgeGroup
import Json.Decode exposing (Decoder, float, string)
import Json.Decode.Pipeline

type alias Vaccine =
    { days: List VaccineDay
    , today: Maybe VaccineDay --I am super going to regret this
    }

type alias VaccineDay =
    { dateStr: String
    , date: Float
    , residents: VaccineGroup
    , totalVolume: Float
    , under18first: Float
    , under18final: Float
    , from18to29first: Float
    , from18to29final: Float
    , from30to39first: Float
    , from30to39final: Float
    , from40to49first: Float
    , from40to49final: Float
    , from50to59first: Float
    , from50to59final: Float
    , from60to69first: Float
    , from60to69final: Float
    , from70to79first: Float
    , from70to79final: Float
    , over80first: Float
    , over80final: Float
    }

type alias VaccineGroup =
    { rawDoses: Float
    , rawFirstDoses: Float
    , rawFinalDoses: Float
    , percentFirst: Float 
    , percentFinal: Float 
    }

decoder: Decoder VaccineDay
decoder = Json.Decode.succeed VaccineDay
        |> Json.Decode.Pipeline.required "dateStr" string
        |> Json.Decode.Pipeline.required "date" float
        |> Json.Decode.Pipeline.required "residents" vaccineGroupDecoder
        |> Json.Decode.Pipeline.required "totalVolume" float
        |> Json.Decode.Pipeline.required "under18first" float
        |> Json.Decode.Pipeline.required "under18final" float
        |> Json.Decode.Pipeline.required "from18to29first" float
        |> Json.Decode.Pipeline.required "from18to29final" float
        |> Json.Decode.Pipeline.required "from30to39first" float
        |> Json.Decode.Pipeline.required "from30to39final" float
        |> Json.Decode.Pipeline.required "from40to49first" float
        |> Json.Decode.Pipeline.required "from40to49final" float
        |> Json.Decode.Pipeline.required "from50to59first" float
        |> Json.Decode.Pipeline.required "from50to59final" float
        |> Json.Decode.Pipeline.required "from60to69first" float
        |> Json.Decode.Pipeline.required "from60to69final" float
        |> Json.Decode.Pipeline.required "from70to79first" float
        |> Json.Decode.Pipeline.required "from70to79final" float
        |> Json.Decode.Pipeline.required "over80first" float
        |> Json.Decode.Pipeline.required "over80final" float
        --|> log "Vaccine"

update: Vaccine -> List VaccineDay -> Vaccine
update initial days =
    case List.head days of
        Just day ->
            upToNow days day
                |> \d -> { initial | today = Just d, days = days }
        Nothing ->
            initial --whatever. don't judge me. 
            
------------------------------------------------

{- what the heck ever I give up on naming things -}
percent: Float -> List Float -> Float
percent divisor summable =
    List.foldl (+) 0 summable
        |> \sum -> sum / divisor
        |> \ratio -> ratio * 100
        
upToNow: List VaccineDay -> VaccineDay -> VaccineDay
upToNow days day =
    { day | under18first = List.map (\d -> d.under18first) days
    |> percent AgeGroup.under18
    , under18final = List.map (\d -> d.under18final) days
    |> percent AgeGroup.under18
    , from18to29first = List.map (\d -> d.from18to29first) days
    |> percent AgeGroup.from18to29
    , from18to29final = List.map (\d -> d.from18to29final) days
    |> percent AgeGroup.from18to29
    , from30to39first = List.map (\d -> d.from30to39first) days
    |> percent AgeGroup.from30to39
    , from30to39final = List.map (\d -> d.from30to39final) days
    |> percent AgeGroup.from30to39
    , from40to49first = List.map (\d -> d.from40to49first) days
    |> percent AgeGroup.from40to49
    , from40to49final = List.map (\d -> d.from40to49final) days
    |> percent AgeGroup.from40to49
    , from50to59first = List.map (\d -> d.from50to59first) days
    |> percent AgeGroup.from50to59
    , from50to59final = List.map (\d -> d.from50to59final) days
    |> percent AgeGroup.from50to59
    , from60to69first = List.map (\d -> d.from60to69first) days
    |> percent AgeGroup.from60to69
    , from60to69final = List.map (\d -> d.from60to69final) days
    |> percent AgeGroup.from60to69
    , from70to79first = List.map (\d -> d.from70to79first) days
    |> percent AgeGroup.from70to79
    , from70to79final = List.map (\d -> d.from70to79final) days
    |> percent AgeGroup.from70to79
    , over80first = List.map (\d -> d.over80first) days
    |> percent AgeGroup.over80
    , over80final = List.map (\d -> d.over80final) days
    |> percent AgeGroup.over80
    }
    


    
vaccineGroupDecoder: Decoder VaccineGroup
vaccineGroupDecoder = Json.Decode.succeed VaccineGroup
        |> Json.Decode.Pipeline.required "rawDoses" float
        |> Json.Decode.Pipeline.required "rawFirstDoses" float
        |> Json.Decode.Pipeline.required "rawFinalDoses" float
        |> Json.Decode.Pipeline.required "percentFirst" float
        |> Json.Decode.Pipeline.required "percentFinal" float
      --|> log "Group"


log : String -> Decoder a -> Decoder a
log message =
    Json.Decode.map (Debug.log message)

init: Vaccine
init =
    { days = []
    , today = Nothing
    }

