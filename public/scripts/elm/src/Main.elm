module Main exposing (main)

import AboutView
import Browser
import ByTest
import ByTestView
import Hospitalization exposing (Hospitalization)
import HospitalizationView
import Html exposing (..)
import Html.Attributes exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, list)
import Models exposing (Model, Mode(..))
import Msg exposing (..)
import Ports
import RawTestDay exposing (RawTestDay)
import Task
import Time
import ToolbarView
import Vaccine
import VaccineView

main : Program () Model Msg
main = Browser.element
       { init = init
       , subscriptions = subscriptions
       , update = update
       , view = view
       }

------------------------
init : () -> (Model, Cmd Msg)
init _ = ( Models.init, Cmd.none )

         
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Ports.updateByTests UpdateByTest
              , Ports.updateHospitalizations UpdateHospitalizationDays
              , Ports.updateVaccines UpdateVaccines 
              ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        SetMode mode ->
            { model | mode = mode } |> \m -> (m, Cmd.none)
        UpdateByTest v ->
            case Decode.decodeValue (Decode.list ByTest.decoder) v of
                Ok days ->
                    { days = List.sortBy .date days |> List.reverse }
                        |> \bt -> { model | byTest  = bt }
                        |> \m -> (m, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                          |> \str -> (model, Cmd.none)
        UpdateHospitalizationDays v ->
            case Decode.decodeValue (Decode.list Hospitalization.decoder) v of
                Ok hospitalizations ->
                    ({model | hospitalizations = hospitalizations }, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                          |> \str -> (model, Cmd.none)
        UpdateVaccines v ->
            case Decode.decodeValue (Decode.list Vaccine.decoder) v of
                Ok days -> List.sortBy .date days |> List.reverse
                        |> \ds -> Vaccine.update model.vaccine ds
                        |> \vaccine -> { model | vaccine = vaccine }
                        |> \m -> (m, Cmd.none)
                Err error -> Debug.toString error |> Debug.log
                             |> \str -> (model, Cmd.none)
        UpdateZone zone -> ({ model | zone = zone }, Cmd.none)


view : Model -> Html Msg
view model =
    div [ class "elm-parent" ]
        [ ToolbarView.root model
        , AboutView.root 
        , case model.mode of
            ByTest ->
                ByTestView.root model
            Models.Hospitalization ->
                HospitalizationView.root model
            Vaccine ->
                VaccineView.root model
        ]

{---------------------------helpers----------------------------}
        
dataPortal : String
dataPortal = "https://data.cityofchicago.org/resource/t4hh-4ku9.json"

rawDaysDecoder : Decoder (List RawTestDay)
rawDaysDecoder = Decode.list RawTestDay.decoder
