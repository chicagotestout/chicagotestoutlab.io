module ByTestTable exposing (root)

import ByTest exposing (TestModel, TestDay)
import Format
import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import Models exposing (Model)

root : Model -> Html Msg
root model =
    div [ class "div-table-wrapper" ]
        [
         table [ class "full-width table" ]
             [ thead []
                   [ th [] [ text "Date" ]
                   , th [] [ text "Day" ]
                   , th [] [ text "Positive Tests" ]
                   , th [] [ text "Total Tests" ]
                   , th [] [ text "Percentage" ]
                   , th [] [ text "Rolling Avg Per 100k Chicagoans" ]
                   ]
             , tbody []
                 (model.byTest.days |> List.map row)
             ]
        ]

row : TestDay -> Html Msg
row day =
    tr []
        [ td [] [ text day.dateString ]
        , td [] [ text day.day ]
        , td [] [ String.fromFloat day.positive |> text ]
        , td [] [ Format.toDecimals 0 day.total |> text ]
        , td [] [ Format.toDecimals 2 day.percent |> text ]
        , td [] [ String.fromFloat day.perCapita |> text ]
        ]
         
