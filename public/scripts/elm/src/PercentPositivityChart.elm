module PercentPositivityChart exposing (root)

import ByTest exposing (TestDay)
import Html exposing (..)
import Html.Attributes exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Axis.Line as AxisLine
import LineChart.Axis.Range as Range
import LineChart.Axis.Ticks as Ticks
import LineChart.Axis.Title as Title
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import Models exposing (Model)
import Time


type alias ChartModel =
    { date: Float
    , val: Float
    }

root: Model -> Html Msg
root model =
    div [ class "div-chart" ]
        [ h3 [] [ text "Percentage of Positive Tests" ]
        , p [ class "p" ]
            [ text "The ratio of positive tests to total tests, "
            , text "expressed as "
            , text "a percentage. "
            ]
        , p [ class "p" ]
            [ text "Source: "
            , a [ href byTestUrl, rel "noopener", target "_blank" ]
                [ Html.cite [] [ text "COVID-19 Daily Testing By Test" ]
                ]
            ]
        , chart model.zone model.byTest.days
        ]

xAxisConfig : Time.Zone -> Axis.Config ChartModel msg
xAxisConfig zone =
    Axis.custom
        { title = Title.default "Time"
        , variable = Just << .date
        , pixels = 1000
        , range = Range.default
        , axisLine = AxisLine.rangeFrame Colors.black
        , ticks = Ticks.time zone 17
        }

chart : Time.Zone -> List TestDay -> Html Msg
chart zone days =
    LineChart.viewCustom
        { x = xAxisConfig zone
        , y = Axis.default 400 "Percent" .val
        , area = Area.default
        , container = Container.default "by-test-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.none
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.wider 2
        , dots = Dots.default
        }
        [ LineChart.line Colors.blue Dots.none "positivity" (points days)
        ]

points: List TestDay -> List ChartModel
points days =
    List.map (\d -> ChartModel d.date d.percent) days


byTestUrl : String
byTestUrl = "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Daily-Testing-By-Test/gkdw-2tgv"
