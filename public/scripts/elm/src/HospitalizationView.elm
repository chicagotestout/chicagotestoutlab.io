module HospitalizationView exposing (root)

import HospitalizationPercentChart
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models exposing (Model)
import Msg exposing (..)

import HospitalizationTable

hospitalizationUrl: String
hospitalizationUrl =
    "https://data.cityofchicago.org/Health-Human-Services/COVID-19-Hospital-Capacity-Metrics/f3he-c6sv"

root : Model -> Html Msg
root model = 
    div [ class "div-hospitalization" ]
        [ text ""
        , h2 [] [ text "Chicago Hospitalizations as a Percentage of "
                , text "Capacity" ]
        , p [ class "p" ]
            [ text "This includes both COVID-19 and non-COVID-19 "
            , text "patients."
            ]
        , p []
            [ text "Source: "
            , a [ href hospitalizationUrl
                , rel "noopener"
                , target "_blank"
                ]
                [ Html.cite [] [ text "COVID-19 Hospital Capacity Metrics"
                               ]
                ]
            ]

        , div [ class "div-chart"]
            [ HospitalizationPercentChart.chart
                  model.zone
                  model.hospitalizations 
            ]
        , HospitalizationTable.root model.hospitalizations
        ]
