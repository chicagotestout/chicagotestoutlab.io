module AgeGroup exposing (..)

{- Chicago populations by age group. 
 - source: https://data.cityofchicago.org/Health-Human-Services/Chicago-Population-Counts/85cm-7uqa -}

under18: Float
under18 = 548999.0

from18to29: Float
from18to29 = 552935.0

from30to39: Float
from30to39 = 456321.0

from40to49: Float
from40to49 = 336457.0

from50to59: Float
from50to59 = 312965.0

from60to69: Float
from60to69 = 262991.0

from70to79: Float
from70to79 = 155334.0

over80: Float
over80 = 79986

