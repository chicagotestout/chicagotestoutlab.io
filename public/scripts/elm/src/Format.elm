module Format exposing (toDecimals)

import FormatNumber
import FormatNumber.Locales exposing (Decimals(..), Locale, usLocale)

toDecimals: Int -> Float -> String
toDecimals places n =
    FormatNumber.format { usLocale | decimals = Exact places
                        , thousandSeparator = ","
                        } n
