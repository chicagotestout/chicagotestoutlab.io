port module Ports exposing (..)

import Json.Decode exposing ( Value )

port updateByTests : (Value -> msg) -> Sub msg

port updateHospitalizations : (Value -> msg) -> Sub msg

port updateVaccines: (Value -> msg) -> Sub msg
