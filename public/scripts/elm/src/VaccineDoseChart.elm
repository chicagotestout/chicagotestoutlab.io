module VaccineDoseChart exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import LineChart
import LineChart.Colors as Colors
import LineChart.Junk as Junk
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Axis.Line as AxisLine
import LineChart.Axis.Range as Range
import LineChart.Axis.Ticks as Ticks
import LineChart.Axis.Title as Title
import LineChart.Junk as Junk
import LineChart.Dots as Dots
import LineChart.Grid as Grid
import LineChart.Dots as Dots
import LineChart.Line as Line
import LineChart.Colors as Colors
import LineChart.Events as Events
import LineChart.Legends as Legends
import LineChart.Container as Container
import LineChart.Interpolation as Interpolation
import LineChart.Axis.Intersection as Intersection
import Msg exposing (..)
import Models exposing (Model)
import Time
import Vaccine exposing (VaccineDay)

type alias ChartModel =
    { date: Float
    , val: Float
    }

root: Model -> Html Msg
root model =
    div [ class "div-chart" ]
        [ h3 [ class "h3" ] [ text "Vaccine Doses by Day" ]
        , p [ class "p" ]
            [ text "Vaccination volume per day over time." ]
        , chart model.zone model.vaccine.days
        ]

chart : Time.Zone -> List VaccineDay -> Html Msg
chart zone days =
    LineChart.viewCustom
        { x = xAxisConfig zone
        , y = Axis.default 400 "Volume" .val
        , area = Area.default
        , container = Container.default "vaccine-dose-chart"
        , interpolation = Interpolation.default
        , intersection = Intersection.default
        , legends = Legends.default
        , events = Events.default
        , junk = Junk.default
        , grid = Grid.default
        , line = Line.wider 2
        , dots = Dots.default
        }
        [ LineChart.line Colors.red Dots.none "first doses"
            (firstPoints days)
        , LineChart.line Colors.black Dots.none
              "total doses" (totalPoints days)
        ]


firstPoints: List VaccineDay -> List ChartModel
firstPoints days =
    List.map (\d -> ChartModel d.date d.residents.rawFirstDoses) days
              
totalPoints: List VaccineDay -> List ChartModel
totalPoints days = List.map (\d -> ChartModel d.date d.residents.rawDoses) days

xAxisConfig : Time.Zone -> Axis.Config ChartModel msg
xAxisConfig zone =
    Axis.custom
        { title = Title.default "Time"
        , variable = Just << .date
        , pixels = 1000
        , range = Range.default
        , axisLine = AxisLine.rangeFrame Colors.black
        , ticks = Ticks.time zone 4
        }
