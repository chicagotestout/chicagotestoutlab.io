module Hospitalization exposing (Hospitalization, decoder)

import Json.Decode exposing (Decoder, float, int, string)
import Json.Decode.Pipeline

type alias Hospitalization =
    { date_string: String
    , date: Float
    , ventilatorsTotalCapacity: Float 
    , ventilatorsTotal: Float
    , ventilatorsCovid19: Float
    , icuBedsTotalCapacity: Float
    , icuBedsTotal: Float
    , icuBedsCovid19: Float
    , acuteNonIcuBedsTotalCapacity: Float
    , acuteNonIcuBedsTotal: Float
    , acuteNonIcuBedsCovid19: Float
    }

decoder : Decoder Hospitalization
decoder = Json.Decode.succeed Hospitalization
    |> Json.Decode.Pipeline.required "date_string" string
    |> Json.Decode.Pipeline.required "date" float
    |> Json.Decode.Pipeline.optional "ventilatorsTotalCapacity" float 0
    |> Json.Decode.Pipeline.required "ventilatorsTotal" float
    |> Json.Decode.Pipeline.required "ventilatorsCovid19" float
    |> Json.Decode.Pipeline.required "icuBedsTotalCapacity" float
    |> Json.Decode.Pipeline.required "icuBedsTotal" float
    |> Json.Decode.Pipeline.required "icuBedsCovid19" float
    |> Json.Decode.Pipeline.required "acuteNonIcuBedsTotalCapacity" float
    |> Json.Decode.Pipeline.required "acuteNonIcuBedsTotal" float
    |> Json.Decode.Pipeline.required "acuteNonIcuBedsCovid19" float
    --|> log "Hospitalization: "


log : String -> Decoder a -> Decoder a
log message =
    Json.Decode.map (Debug.log message)
