module VaccineTable exposing (root)

import Format
import FormatNumber
import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import Models exposing (Model)
import Vaccine exposing (VaccineDay)

root : Model -> Html Msg
root model =
    div [ class "div-table-wrapper" ]
        [
         table [ class "full-width table" ]
             [ thead []
                   [ th [] [ text "Date" ]
                   , th [] [ text "Total Doses Given" ]
                   , th [] [ text "First Doses" ]
                   , th [] [ text "Final Doses" ]
                   , th [] [ text "Percent Chicagoans Vaccinated" ]
                   , th [] [ text "Percent Chicagoans Fully Vaccinated" ]
                   ]
             , tbody []
                 (model.vaccine.days |> List.map row)
             ]
        ]

row : VaccineDay -> Html Msg
row day =
    tr []
        [ td [] [ text day.dateStr ]
        , td [] [ String.fromFloat day.residents.rawDoses |> text ]
        , td [] [ String.fromFloat day.residents.rawFirstDoses |> text ]
        , td [] [ String.fromFloat day.residents.rawFinalDoses |> text ]
        , td [] [ day.residents.percentFirst |> Format.toDecimals 1 |> text ]
        , td [] [ day.residents.percentFinal |> Format.toDecimals 1 |> text ]
        ]
         
