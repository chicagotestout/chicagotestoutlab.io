cd scripts/ &&
    npm i &&
    cd elm/ &&
    npx elm make src/Main.elm --output=index.js &&
    cd ../rescript/ &&
    npm i &&
    npm run build &&
    cd ../wkr/ &&
    npm i &&
    npm run build
