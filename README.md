# README for [Chicago Test Out](https://chicagotestout.gitlab.io/)

## Purpose 

This project was originally written to fulfill my own need to check the 
relevant COVID-19 pandemic metrics daily and understand their change over time.
Using official City of Chicago data, it displays the same metrics used to 
determine City of Chicago policies. I use it personally to help make decisions
in my own life about which risks are acceptable, and I hope others find it 
useful as well.

## Project Milestones
(*Dates are approximate because releases are on a rolling basis across brief
sprints.*)

- [x] wrote basic *By Person* table and chart--*Memorial Day weekend, 2020*
- [x] added hospitalization chart and table to track hospitalizations on 
and absolute level and relative to capacity.--*late July*
- [x] replaced original *By Person* tab with a new tab based on *By Test* data
([more details here](https://webbureaucrat.gitlab.io/posts/chicago-test-out-moving-to-by-test-data/)). --*mid-October, 2020*
- [x] improve stability in the hospitalization tabs with a robust parser. 
--*mid-November, 2020*
- [x] add vaccination tab. --*New Year's, 2021*
- [x] break vaccination data down by age. --*MLK, 2021*

## Building & Running the project

This project requires npm to be installed. 

The *public/* subfolder contains a script *build.sh* This runs the build
scripts for both *public/scripts/elm/* and *public/scripts/rescript/*. 

The script needs a static server. Personally, I use 
[`https-server`](https://www.npmjs.com/package/http-server). 

```sh
sudo npm install -g http-server
```

But use what you want to run it.

### A word on the elm build

If you open up a console on the website, you might notice the console 
warning from elm, saying that the project was compiled for development and 
suggesting production optimizations. 

Unfortunately, in order to compile for production, I'd have to remove all
the console logging the elm project, and I can't justify that for a couple
of reasons. One is that occasionally I like to have debug logs in production
to help diagnose production issues. Another is that I'm practicing continuous
deployment and can't really justify removing all the debug logs before every 
push. 

Fortunately, I think the site is pretty fast already. I don't think these
optimizations are very important right now. 

## TODOs
- [x] write Vaccinations tab
- [x] write local automation script

## Credits

Icon based on photo: 

<p style="font-size: 0.9rem;font-style: italic;">
    <img style="display: block; max-width: 100px;" 
    src="https://live.staticflickr.com/3103/2798118074_c329b6f624_b.jpg" alt="Chicago skyline (horizontal)"><a href="https://www.flickr.com/photos/75138551@N00/2798118074">"Chicago skyline (horizontal)"</a><span> by <a href="https://www.flickr.com/photos/75138551@N00">rogersmj</a></span> is licensed under <a href="https://creativecommons.org/licenses/by-nc/2.0/?ref=ccsearch&atype=html" style="margin-right: 5px;">CC BY-NC 2.0</a><a href="https://creativecommons.org/licenses/by-nc/2.0/?ref=ccsearch&atype=html" target="_blank" rel="noopener noreferrer" style="display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;"><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.creativecommons.org/static/img/cc_icon.svg?image_id=e322b9a5-5384-4f4e-a90f-4854fc8ddaa0" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.creativecommons.org/static/img/cc-nc_icon.svg" /></a></p>
